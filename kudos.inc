<?php
//
//

/**
 * Callback function for admin/content/kudos. Display the configuration form.
 */
function kudos_admin_config_page($kudo_id = -1) {
  $content = "";

  if ($kudo_id <= 0) {
    $content .= _kudos_admin_config_listing();
  }
  $content .= drupal_get_form('kudos_admin_config_form', $kudo_id);

  return $content;
}//end - function

/**
 * Implementation of hook_form()
 *
 * Returns an individual specific for an individual node
 */
function kudos_widget_form($content_type, $content_id, $widget_style = 'default') {
  $content = '';

  //if user can view kudos
  if (user_access('view kudos')) {
    $content = drupal_get_form('kudos_form_'. $content_type .'_' . $content_id, $content_type, $content_id, $widget_style);

    //switch on the style of the widget
    switch ($widget_style) {
      case 'block' :
        $content = theme('kudos_buttons_block', $content);
        break;
      default :
        $content = theme('kudos_buttons', $content);
        break;
    }//end - switch on style for the widget
  }//end - if user can view kudos

  //this indirectly does a callback to the kudos_form function
  return $content;
}//end - function

/**
 * Implementation of theme_kudos_image()
 *
 * Default themeing for an individual kudos image.
 */
function _kudos_button_image($vars) {
  return theme('kudos_button_image', $vars);
}//end - function

/**
 * Implementation of theme_kudos_icon()
 *
 * Default themeing for an individual kudos icon.  This is implemented as an input type image
 */
function _kudos_button_icon($vars) {
  return theme('kudos_button_icon', $vars);
}//end - function

/**
 * Generate a summary report based on a cached summary that was already generated
 *
 * @param $report_label - what to call the new report
 * @param $period - the currently cached data
 * @param $content_type - [optional] the content type that should be included (should be 'node', 'comment' or 'user')
 */
function kudos_generate_summary_report($report_label, $period, $content_type) {
  $response = _kudos_cache_summary($report_label, $period, $content_type);

  //if a summary report was generated
  if ($response) {
    //retrieve the summary
    $results = _kudos_get_content_summary('node', $report_label);

    //invoke the hook so that other modules may grant points, notifications, etc
    $rc = module_invoke_all('kudos_summaryapi', $report_label, $period, $content_type, $results);
  }//end - if a summary report was generated

  return $response;
}//end - function

/**
 * Retrieves the themed version of full kudos account stats
 *
 * @param $a1 - this can be either an array of kudos stats as pulled from _kudos_get_account_stats(uid)
 *              OR
 *              the UID of the user
 */
function kudos_account_stats($a1, $period = 'all') {
  $_stats = FALSE;
  $content = '';

  //if user has permissions to view kudos
  if (user_access('view kudos')) {
    //ensure that we have valid stats to work with
    if (is_array($a1)) {
      $_stats = $a1;
    } else if (is_numeric($a1) && $a1 > 0) {
      $_stats = _kudos_get_account_stats($a1, $period);
    }

    //if we found stats
    if ($_stats && is_array($_stats) && count($_stats) > 0) {
      $vars = array();
      $items = array();
      foreach ($_stats as $key => $details) {
        $vars['name'] = $key;
        $vars['details'] = $details;
        $items[] = theme('kudos_account_stats_item', $vars);
      }
      $vars = array();
      $vars['items'] = $items;
      $content = theme('kudos_account_stats', $vars);
    }//end - if we found stats
  }//end - if user has permissions
  return $content;
}//end - function

/**
 * Retrieves the themed version of full kudos account summary (top x report)
 *
 * @param $period - the period that we're pulling the stats from
 */
function kudos_account_summary($period = 'all') {
  $_stats = FALSE;
  $content = '';

  //if user has permissions to view kudos
  if (user_access('view kudos')) {
    //ensure that we have valid stats to work with
    if (is_array($a1)) {
      $_stats = $a1;
    } else {
      $_stats = _kudos_get_account_summary($period);
    }

    //if we found stats
    if ($_stats && is_array($_stats) && count($_stats) > 0) {
      $vars = array();
      $items = array();
      foreach ($_stats as $key => $details) {
        $vars['name'] = $key;
        $vars['details'] = $details;
        $items[] = theme('kudos_account_summary_item', $vars);
      }
      $vars = array();
      $vars['items'] = $items;
      $content = theme('kudos_account_summary', $vars);
    }//end - if we found stats
  }//end - if user has permissions
  return $content;
}//end - function

/**
 * Retrieves the themed version of content specific kudos stats
 *
 * @param $content_type - the type of content we are getting stats for. this should be 'node' or 'comment'
 * @param $content_id - the id of the piece of content that we are getting stats for
 * @param $period - the period for which we want stats.  This must be a period that is being cached
 */
function kudos_content_stats($a1, $content_id = 0, $period = 'all') {
  $_stats = FALSE;
  $content = '';
  //if user has permissions to view kudos
  if (user_access('view kudos')) {

    if (is_array($a1) && count($a1) > 0) {
      $_stats = $a1;
    } else if ($a1 !== "" && is_numeric($content_id) && $content_id > 0) {
      $_stats = _kudos_get_content_stats($a1, $content_id, $period);
    }

    //if we found stats
    if ($_stats && is_array($_stats) && count($_stats) > 0) {
      $vars = array();
      $items = array();
      foreach ($_stats as $key => $details) {
        $vars['name'] = $key;
        $vars['details'] = $details;
        $items[] = theme('kudos_content_stats_item', $vars);
      }
      $vars = array();
      $vars['items'] = $items;
      $content = theme('kudos_content_stats', $vars);
    }//end - if we found stats
  }//end - if user has permissions
  return $content;
}//end - function

/**
 * Retrieves the themed version of content specific kudos stats
 *
 * @param $content_type - the type of content we are getting stats for. this should be 'node' or 'comment'
 * @param $period - the period for which we want stats.  This must be a period that is being cached
 */
function kudos_content_summary($a1, $period = 'all') {
  $content_id = FALSE; //this is a summary report, so we don't need the content_id
  $_stats = FALSE;
  $content = '';
  //if user has permissions to view kudos
  if (user_access('view kudos')) {

    if (is_array($a1) && count($a1) > 0) {
      $_stats = $a1;
    } else if ($a1 !== "") {
      $_stats = _kudos_get_content_summary($a1, $period);
    }

    //if we found stats
    if ($_stats && is_array($_stats) && count($_stats) > 0) {
      $vars = array();
      $items = array();
      foreach ($_stats as $key => $details) {
        $vars['name'] = $key;
        $vars['details'] = $details;
        $items[] = theme('kudos_content_summary_item', $vars);
      }
      $vars = array();
      $vars['items'] = $items;
      $content = theme('kudos_content_summary', $vars);
    }//end - if we found stats
  }//end - if user has permissions
  return $content;
}//end - function

/**
 * Helper function to retrieve the kudos that are available to a given user/piece of content
 **/
function _kudos_retrieve($uid = -1, $content_type = '', $content_id = 0, $status = 1) {
  $response = FALSE;
  $_content = FALSE;
  $_kudos_enabled = TRUE;

  //if user may not give kudos
  if (! user_access('give kudos')) {
    $_kudos_enabled = FALSE;
  }//end - if user may not give kudos

  //retrieve info on the content being kudo'd
  $_content = _kudos_get_content_info($content_type, $content_id);

  //if the author is not the same as the viewer then don't allow user to pat themselves on the back
  if ($_kudos_enabled && $_content['uid'] > 0 && $uid == $_content['uid']) {
    $_kudos_enabled = FALSE;
  }

  //if we restrict which content authors may receive kudos
  if ($_kudos_enabled && variable_get('kudos_restrict_receive','0')) {
    if (is_numeric($_content['uid']) && $_content_author = user_load($array = array('uid' => $_content['uid']))) {
      if (! user_access('receive kudos', $_content_author)) {
        $_kudos_enabled = FALSE;
      }
    }//end - get content author info
  }//end - if we restrict which content authors may receive kudos


  $params = array();
  //build query to retrieve the available kudos
  $query = " SELECT i.id, i.name, i.label, i.status, i.comments, i.icon_give, i.icon_rescind, i.icon_disabled ";
  if ($uid > 0) {
    $query .= ", (SELECT 1 FROM {kudos_granted} g WHERE g.kudo_id = i.id and g.content_type = '%s' AND g.content_id = %d AND g.uid = %d) as given ";
    $params[] = $content_type;
    $params[] = $content_id;
    $params[] = $uid;
  } else {
    $query .= ", 0 as given ";
  }
  $query .= " FROM {kudos_info} i ";
  $where = "";

  //if this is a node, make sure that its content type is allowed
  if ($content_type === "node") {
    if (!$_content) {
      //retrieve info on the content being kudo'd
      $_content = _kudos_get_content_info($content_type, $content_id);
    }
    $_type = $_content['type'];
    if ($_type !== "") {
      $query .= " JOIN {kudos_info_types} t ON i.id = t.kudo_id AND t.node_type = '%s' ";
      $params[] = $_type;
    }
  }//end - if

  //if this is a comment, make sure that comments should be shown
  if ($content_type === "comment") {
    $where = ($where !== "" ? " AND " : "") . " i.comments = %d ";
    $params[] = 1;
  }//end - if

  //if we want to filter by status
  if ($status && is_numeric($status)) {
    $where .= ($where !== "" ? " AND " : "") . " i.status = " . $status;
  }

  //if we have a where clause to add
  if ($where !== "") {
    $query .= " WHERE " . $where;
  }

  $result = db_query($query, $params);

  //while we have kudos that can be displayed
  while ($row = db_fetch_array($result)) {
    if (!$response) { $response = array(); }

    $response[$row['name']] = array();
    $response[$row['name']]['name'] = $row['name'];
    $response[$row['name']]['id'] = $row['id'];
    $response[$row['name']]['enable'] = ($_kudos_enabled ? 'YES' : 'NO');//if the author of this content may receive kudos
      $_status = ($row['given'] == 0 ? 'give' : 'rescind'); //determine if the user should be giving or rescinding this kudo
      $_status = ($_kudos_enabled ? $_status : 'none');//if the user is disabled from giving a kudo, then show the disabled state
    $response[$row['name']]['status'] = $_status;
    $response[$row['name']]['label'] = $row['label'];
    $response[$row['name']]['content_types'] = _kudos_retrieve_types($row['id']);
    $response[$row['name']]['icon']['give'] = $row['icon_give'];
    $response[$row['name']]['icon']['rescind'] = $row['icon_rescind'];
    $response[$row['name']]['icon']['disabled'] = $row['icon_disabled'];
  }//end - while

  return $response;
}//end - function

/**
 * Helper function to retrieve the kudos that are available to a given user/piece of content
 **/
function _kudos_retrieve_types($kudo_id) {
  $response = FALSE;

  $query = " SELECT kudo_id, node_type FROM {kudos_info_types} WHERE kudo_id = %d ";
  $params = array();
  $params[] = $kudo_id;
  $result = db_query($query, $params);

  //while there are types
  while ($row = db_fetch_array($result)) {
    if (!$response) {
      $response = array();
    }
    $response[$row['node_type']] = $row['node_type'];
  }//end - while

  return $response;
}//end - function

/**
 * Helper function that will clear a previously existing cached summary
 **/
function _kudos_cache_summary_clear($report_label) {
  $response = FALSE;

  //clear out any existing summary with the given period value
  $query = " DELETE FROM {kudos_cached} WHERE period = '%s' ";
  $params = array();
  $params[] = $report_label;
  $result = db_query($query, $params);

  if (db_affected_rows() > 0) {
    $result = TRUE;
  }

  return $response;
}//end - function

/**
 * Helper function that will populate a summary report based on a full cached result set
 **/
function _kudos_cache_summary($report_label, $period, $content_type = FALSE) {
  $_tmp = _kudos_cache_summary_clear($report_label);

  //recreate the summary report
  $_wheres = array();
  $params = array();

  $params[] = $report_label;
  if ($content_type) {
    $_wheres[] = "content_type = '%s'";
    $params[] = $content_type;
  }
  $_wheres[] = "period = '%s'";
  $params[] = $period;

  $_where_claus = implode(" AND ", $_wheres);

  $query = "
    INSERT INTO {kudos_cached} (kudo_id, content_type, content_id, num_given, period)
    SELECT kudo_id, content_type, content_id, max( num_given ) AS num_given, '%s' as period
    FROM {kudos_cached}
    WHERE ". $_where_claus ."
    GROUP BY kudo_id
  ";

  $result = db_query($query, $params);
  if (db_affected_rows() > 0) {
    $response = TRUE;
  }

  return $response;
}//end - function

/**
 * Helper function that will clear previously cached data from a given period
 **/
function _kudos_cache_update_clear($period = 'all', $content_type = 'user', $content_id = 0) {
  $response = FALSE;

  //clear the cache for this period
  if ($content_id > 0) {
    $query = " DELETE FROM {kudos_cached} WHERE content_type = '%s' AND content_id = %d AND period = '%s' ";
    $params = array();
    $params[] = $content_type;
    $params[] = $content_id;
    $params[] = $period;
  } else {
    $query = " DELETE FROM {kudos_cached} WHERE content_type = '%s' AND period = '%s' ";
    $params = array();
    $params[] = $content_type;
    $params[] = $period;
  }
  $result = db_query($query, $params);

  if (db_affected_rows() > 0) {
    $response = TRUE;
  }

  return $response;
}//end - function

/**
 * Helper function that will populate the kudos cache table so that status do not have to be calculated each time
 **/
function _kudos_cache_update($period = 'all', $content_type = 'user', $content_id = 0) {
  $response = FALSE;
  $skip = FALSE;

  $limit = 0; //this is the number of top rows we will return.  zero for all

  $_tmp = _kudos_cache_update_clear($period, $content_type, $content_id);

  //check more advanced handling of periods (ie. data ranges)
  $period_info = _kudos_get_cache_period_info($period);

  //if the current period is not one we should be worrying about caching, then skip the process
  if (! in_array($content_type, $period_info['content_types'])) {
    $skip = TRUE;
  }
  $limit = (isset($period_info['limit']) ? $period_info['limit'] : $limit);

  //common additions to the standard select queries generated below
  $query_insert = " INSERT INTO {kudos_cached} (kudo_id, content_type, content_id, num_given, period) ";
  $query_insert_values = " VALUES (%d, '%s', %d, %d, '%s') ";
  $query_orderby = " ORDER BY num_given desc ";

  //switch on type of cache to repopulate for this period
  switch ($content_type) {
    case 'user' :
      $_where = '';
      $wheres = array();
      $wheres_params = array();

      //if we have period specific information, handle the date specifics here
      if ($period_info && isset($period_info['date_from']) && isset($period_info['date_to'])) {
        $wheres[] = " g.created >= '%s' AND g.created < '%s' ";
        $wheres_params[] = $period_info['date_from']; //for node subquery
        $wheres_params[] = $period_info['date_to'];
        $wheres_params[] = $period_info['date_from']; //for comment subquery
        $wheres_params[] = $period_info['date_to'];
      } else if ($period !== "all") {
        $skip = TRUE;
      }

      //since this is a query to get user stats, we need to merge all the content that an individual owns
      $_wheres_node = $_where;
      $_wheres_comment = $_where;

      if ($content_id > 0) {
        $_wheres_node[] = " n.uid = %d ";
        $_wheres_comment[] = " c.uid = %d ";

        $wheres_params[] = $content_id; //node subquery
        $wheres_params[] = $content_id; //comment subquery
      }

      //merge the where clause
      $_where_node = '';
      $_where_comment = '';
      if (count($_wheres_node) > 0) { $_where_node = " WHERE " . implode(' AND ', $_wheres_node); }
      if (count($_wheres_comment) > 0) { $_where_comment = " WHERE " . implode(' AND ', $_wheres_comment); }

      //reinsert the data for this period
      $query = "
        SELECT kudo_id, '%s' as content_type, uid as content_id, count(1) as num_given, '%s' as period
        FROM (
          SELECT g.kudo_id, n.uid
          FROM {kudos_granted} g
          JOIN {node} n ON g.content_type = 'node' AND g.content_id = n.nid
          ". $_where_node ."

          UNION ALL

          SELECT g.kudo_id, c.uid
          FROM {kudos_granted} g
          JOIN {comments} c ON g.content_type = 'comment' AND g.content_id = c.cid
          ". $_where_comment ."
          ) collection
        GROUP BY kudo_id, content_type, content_id
      ";
      $params = array();
      $params[] = $content_type;
      $params[] = $period;

      if (count($wheres_params) > 0) { $params = array_merge($params, $wheres_params); }

      break;
    case 'node' :
      $_where = '';
      $wheres = array();
      $wheres_params = array();

      //narrow down by a specific piece of content
      if ($content_id > 0) {
        $wheres[] = " g.content_id = %d ";
        $wheres_params[] = $content_id;
      }

      //if we have period specific information, handle the date specifics here
      if ($period_info && isset($period_info['date_from']) && isset($period_info['date_to'])) {
        $wheres[] = " g.created >= '%s' AND g.created <= '%s' ";
        $wheres_params[] = $period_info['date_from'];
        $wheres_params[] = $period_info['date_to'];
      } else if ($period !== "all") {
        $skip = TRUE;
      }

      //merge the where clause
      if (count($wheres) > 0) {
        $_where = " WHERE " . implode(' AND ', $wheres);
      }

      //reinsert the data for this period
      $query = "
        SELECT g.kudo_id, 'node' as content_type, g.content_id as content_id, count(1) as num_given, '%s' as period
        FROM {kudos_granted} g
        JOIN {node} n ON g.content_type = 'node' AND g.content_id = n.nid
        ". $_where ."
        GROUP BY kudo_id, content_type, content_id
      ";
      $params = array();
      $params[] = $period;

      if (count($wheres_params) > 0) { $params = array_merge($params, $wheres_params); }

      break;
    case 'comment' :
      $_where = '';
      $wheres = array();
      $wheres_params = array();

      //narrow down by a specific piece of content
      if ($content_id > 0) {
        $wheres[] = " g.content_id = %d ";
        $wheres_params[] = $content_id;
      }

      //if we have period specific information, handle the date specifics here
      if ($period_info && isset($period_info['date_from']) && isset($period_info['date_to'])) {
        $wheres[] = " g.created >= '%s' AND g.created <= '%s' ";
        $wheres_params[] = $period_info['date_from'];
        $wheres_params[] = $period_info['date_to'];
      } else if ($period !== "all") {
        $skip = TRUE;
      }

      //merge the where clause
      if (count($wheres) > 0) {
        $_where = " WHERE " . implode(' AND ', $wheres);
      }

      //reinsert the data for this period
      $query = "
        SELECT g.kudo_id, 'comment' as content_type, g.content_id as content_id, count(1) as num_given, '%s' as period
        FROM {kudos_granted} g
        JOIN {comments} c ON g.content_type = 'comment' AND g.content_id = c.cid
        ". $_where ."
        GROUP BY kudo_id, content_type, content_id
      ";
      $params = array();
      $params[] = $period;

      if (count($wheres_params) > 0) { $params = array_merge($params, $wheres_params); }

      break;
    default :
      $query = FALSE;
  }//end - switch

  //if we have a valid query
  if ($query && !$skip) {

    //if we want to limit how many entries we insert, then we need to insert individually for database compatibility
    if ($limit > 0) {
      $query = $query . $query_orderby; //since we're only pulling the top x rows, we need to order the values first so that we always get the highest
      $result = db_query_range($query, $params, 0, $limit);
      $success_idx = 0;

      //while we have records to insert
      while ($row = db_fetch_array($result)) {
        $_in_query = $query_insert . $query_insert_values;
        $params = array();
        $params[] = $row['kudo_id'];
        $params[] = $row['content_type'];
        $params[] = $row['content_id'];
        $params[] = $row['num_given'];
        $params[] = $row['period'];
        $_in_result = db_query($_in_query, $params);
        if (db_affected_rows() > 0) {
          $success_idx++;
        }
      }//end - while

      if ($success_idx > 0) { $response = TRUE; }

    } else { //else - we're just inserting the entire thing, so do it in one fell swoop

      //add the insert columns to the query
      $query = $query_insert . $query;

      $result = db_query($query, $params);
      if (db_affected_rows() > 0) {
        $response = TRUE;
      }
    }//end - if we want to limit how many entries
  }//end - if we have a valid query

  return $response;
}//end - function

/**
 * Helper function to retrieve information on a specific kudo
 **/
function _kudos_specs($kudo) {
  $response = FALSE;

  if (is_numeric($kudo)) {
    $query = " SELECT id, name, label, status, comments, icon_give, icon_rescind, icon_disabled FROM {kudos_info} WHERE id = '%d' ";
  } else {
    $query = " SELECT id, name, label, status, comments, icon_give, icon_rescind, icon_disabled FROM {kudos_info} WHERE name = '%s' ";
  }
  $params = array($kudo);
  $result = db_query($query, $params);

  //if found kudo
  if ($row = db_fetch_array($result)) {
    $response = $row;
    $response['icon'] = array();
    $response['icon']['give'] = $row['icon_give'];
    $response['icon']['rescind'] = $row['icon_rescind'];
    $response['icon']['disabled'] = $row['icon_disabled'];
    $response['content_types'] = _kudos_retrieve_types($row['id']);
  }//end - if found kudo

  return $response;
}//end - function

/**
 * Helper function to get stats on what kudos this individual has earned
 **/
function _kudos_get_account_stats($content_id, $period = 'all') {
  $response = FALSE;

  $query = "
    SELECT i.*, c.num_given
    FROM {kudos_cached} c
    JOIN {kudos_info} i ON c.kudo_id = i.id AND i.status = 1
    WHERE
  ";

  $wheres = array();
  $params = array();

  $wheres[] = "c.content_type = 'user'";
  $wheres[] = "c.content_id = %d";
  $params[] = $content_id;
  $wheres[] = "c.period = '%s'";
  $params[] = $period;

  $where_clause = implode(" AND ", $wheres);
  $query .= $where_clause;

  $result = db_query($query, $params);

  $_image_path = variable_get('kudos_filepath','');

  //while we have results
  while ($row = db_fetch_array($result)) {
    //initialize the response to be an array
    if (!$response) {
      $response = array();
    }

    $response[$row['name']] = array();
    $response[$row['name']]['name'] = $row['name'];
    $response[$row['name']]['id'] = $row['id'];
    $response[$row['name']]['label'] = $row['label'];
    $response[$row['name']]['num_given'] = $row['num_given'];
    $response[$row['name']]['icon']['give'] = $_image_path . $row['icon_give'];
    $response[$row['name']]['icon']['rescind'] = $_image_path . $row['icon_rescind'];
    $response[$row['name']]['icon']['disabled'] = $_image_path . $row['icon_disabled'];
  }//end - while

  return $response;
}//end - function

/**
 * Helper function to get summary of top users that received kudos
 *
 * Note: for now this is just a wrapper of the content summary,
 * but this may change in the future if difference between how users and content
 * items are grouped are deemed necessary
 **/
function _kudos_get_account_summary($period = 'all') {
  return _kudos_get_content_summary('user', $period);
}//end - function

/**
 * Helper function to get stats on what kudos a piece of content has earned
 **/
function _kudos_get_content_stats($content_type, $content_id, $period = 'all') {
  $response = FALSE;

  $query = "
    SELECT i.*, c.num_given
    FROM {kudos_cached} c
    JOIN {kudos_info} i ON c.kudo_id = i.id AND i.status = 1
    WHERE
  ";

  $wheres = array();
  $params = array();

  $wheres[] = "c.content_type = '%s'";
  $params[] = $content_type;
  $wheres[] = "c.content_id = %d";
  $params[] = $content_id;
  $wheres[] = "c.period = '%s'";
  $params[] = $period;

  $where_clause = implode(" AND ", $wheres);
  $query .= $where_clause;

  $result = db_query($query, $params);

  $_image_path = variable_get('kudos_filepath','');

  //while we have results
  while ($row = db_fetch_array($result)) {
    //initialize the response to be an array
    if (!$response) {
      $response = array();
    }

    $response[$row['name']] = array();
    $response[$row['name']]['name'] = $row['name'];
    $response[$row['name']]['id'] = $row['id'];
    $response[$row['name']]['label'] = $row['label'];
    $response[$row['name']]['num_given'] = $row['num_given'];
    $response[$row['name']]['icon']['give'] = $_image_path . $row['icon_give'];
    $response[$row['name']]['icon']['rescind'] = $_image_path . $row['icon_rescind'];
    $response[$row['name']]['icon']['disabled'] = $_image_path . $row['icon_disabled'];
  }//end - while

  return $response;
}//end - function

/**
 * Helper function to get a summary of stats on the top kudos for content
 **/
function _kudos_get_content_summary($content_type, $period = 'all', $maxrows = 100) {
  $response = FALSE;

  $query = "
    SELECT i.*, c.content_type, c.content_id, c.num_given
    FROM {kudos_cached} c
    JOIN {kudos_info} i ON c.kudo_id = i.id AND i.status = 1
    WHERE
  ";

  $wheres = array();
  $params = array();

  $wheres[] = "c.content_type = '%s'";
  $params[] = $content_type;
  $wheres[] = "c.period = '%s'";
  $params[] = $period;

  $where_clause = implode(" AND ", $wheres);
  $query .= $where_clause;
  $query .= " ORDER BY c.num_given DESC ";

  $result = db_query_range($query, $params, 0, $maxrows);

  $_image_path = variable_get('kudos_filepath','');

  //while we have results
  while ($row = db_fetch_array($result)) {
    //initialize the response to be an array
    if (!$response) {
      $response = array();
    }

    $response[$row['name']] = array();
    $response[$row['name']]['name'] = $row['name'];
    $response[$row['name']]['content_id'] = $row['content_id'];
    $response[$row['name']]['content_type'] = $row['content_type'];
    $response[$row['name']]['id'] = $row['id'];
    $response[$row['name']]['label'] = $row['label'];
    $response[$row['name']]['num_given'] = $row['num_given'];
    $response[$row['name']]['icon']['give'] = $_image_path . $row['icon_give'];
    $response[$row['name']]['icon']['rescind'] = $_image_path . $row['icon_rescind'];
    $response[$row['name']]['icon']['disabled'] = $_image_path . $row['icon_disabled'];
  }//end - while

  return $response;
}//end - function

/**
 * Helper function to retrieve what periods are currently being processed by this module and any others that hook into it
 **/
function _kudos_get_cache_periods() {
  $response = module_invoke_all('kudos_cache_periods');
  if (!is_array($response)) {
    $response = array($response);
  }
  return $response;
}//end - function

/**
 * Helper function to retrieve more advanced period handling
 **/
function _kudos_get_cache_period_info($period = 'all') {
  $response = FALSE;

  $_period_info = module_invoke_all('kudos_cache_period_info', $period);
  if (isset($_period_info['date_from']) && isset($_period_info['date_to'])) {
    $response = array();
    $response['date_from'] = (is_numeric($_period_info['date_from']) ? date('Y-m-d H:i:s', $_period_info['date_from']) : $_period_info['date_from']);
    $response['date_to'] = (is_numeric($_period_info['date_to']) ? date('Y-m-d H:i:s', $_period_info['date_to']) :$_period_info['date_to']);
  }
  if (isset($_period_info['limit'])) {
    $response['limit'] = $_period_info['limit'];
  }

  if (isset($_period_info['content_types']) && is_array($_period_info['content_types'])) {
    $response['content_types'] = $_period_info['content_types'];
  } else {
    $response['content_types'] = array('node','comment','user');
  }


  return $response;
}//end - function

/**
 * Helper function to determine information on a piece of content... its type, created, uid, etc
 **/
function _kudos_get_content_info($content_type, $content_id) {
  $response = FALSE;
  $query = "";

  //switch on type of content
  switch ($content_type) {
    case 'node' :
      $query = " SELECT
                  'node' as content_type,
                  nid as content_id,
                  nid,
                  uid,
                  type,
                  title,
                  status,
                  created
                  FROM {node} WHERE nid = %d ";
      break;
    case 'comment' :
      $query = " SELECT 'comment' as content_type,
                  cid as content_id,
                  nid,
                  uid,
                  '' as type,
                  subject as title,
                  status,
                  timestamp as created
                  FROM {comments} where cid = %d ";
      break;
  }//end - switch

  //if we have a valid query
  if ($query !== "") {
    $result = db_query($query, $content_id);
    //if found a result
    if ($row = db_fetch_array($result)) {
      $response = $row;
    }//end - if found a result
  }//end - if we have a query

  return $response;
}//end - function

/**
 * Generation of a listing of available kudos that need updating
 **/
function _kudos_admin_config_listing() {
  $vars = _kudos_retrieve(-1, '', 0, FALSE);
  $content = theme('kudos_admin_config_listing', $vars);
  return $content;
}//end - function

/**
 * Helper function that checked is a piece of content has already been granted a specific kudo by a the entered user
 */
function _kudos_check($kudo, $uid, $content_type, $content_id) {
  $response = FALSE;
  //TODO: give option to only allow users to give a kudo n number of times to a specific user

  //determine the id of the kudo that we working with
  $kudo_id = $kudo;
  if (!is_numeric($kudo_id)) {
    $_tmp = _kudos_specs($kudo);
    $kudo_id = (isset($_tmp['id']) ? $_tmp['id'] : FALSE);
  }//end - if not numeric

  //construct and execute the query
  $query = "
            SELECT count(*) as cnt
            FROM {kudos_granted} g, {kudos_info} i
            WHERE g.content_type = '%s'
                  AND g.content_id = '%s'
                  AND g.uid = %d
                  AND g.kudo_id = i.id
                  AND i.id = %d
          ";
  $params = array($content_type, $content_id, $uid, $kudo_id);
  $result = db_query($query, $params);

  $_cnt = 0;

  //if we found info on this kudo
  if ($row = db_fetch_array($result)) {
    $_cnt = $row['cnt'];
  }//end - if we found

  //if user has not given a kudo to this content
  if ($_cnt > 0) { $response = TRUE; }

  return $response;
}//end - function

/**
 * Helper function that handles the awarding of a kudos to a piece of content
 */
function _kudos_give($kudo = '', $uid = FALSE, $content_type = '', $content_id = 0) {
  $response = FALSE;

  //determine what user is giving the kudo
  if (!$uid) {
    global $user;
    $uid = $user->uid;
  }

  //get info on the kudo being given
  $_spec = _kudos_specs($kudo);

  //if kudo found
  if ($_spec) {
    //if user can grant this kudo
    if (!_kudos_check($_spec['id'], $uid, $content_type, $content_id)) {
      $query = "
        INSERT INTO {kudos_granted} (kudo_id, uid, content_type, content_id)
        VALUES (%d, %d, '%s', %d)
      ";
      $params = array($_spec['id'], $uid, $content_type, $content_id);
      $result = db_query($query, $params);
      if (db_affected_rows() > 0) {
        $response = TRUE;
      }
    }//end - if user can grant this kudo
  }//end - if kudo found

  return $response;
}//end - function

/**
 * Helper function that handles the rescinding of a kudos from a specific user on a piece of content
 */
function _kudos_rescind($kudo = '', $uid = FALSE, $content_type = '', $content_id = 0) {
  $response = FALSE;

  //determine what user is giving the kudo
  if (!$uid) {
    global $user;
    $uid = $user->uid;
  }

  //get info on the kudo being given
  $_spec = _kudos_specs($kudo);

  //if kudo found
  if ($_spec) {
    //if user has given this content this kudo already
    if (_kudos_check($_spec['id'], $uid, $content_type, $content_id)) {
      $query = "
        DELETE FROM {kudos_granted} WHERE kudo_id = %d AND uid = %d AND content_type = '%s' AND content_id = %d
      ";
      $params = array($_spec['id'], $uid, $content_type, $content_id);
      $result = db_query($query, $params);
      if (db_affected_rows() > 0) {
        $response = TRUE;
      }
    }//end - if user has given this content this kudo already
  }//end - if kudo found

  return $response;
}//end - function
