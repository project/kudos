//
//

----------------------------------
Basic Implementation Instructions
----------------------------------

There are two areas of configuration.

1) Global settings with is located under Site configuration

    * Here you configure how you want kudos to respond, what content types should have kudos bound to them, and whether people can rescind/take back kudos that they've given out.
    * Currently I have icon and image, but was considering if it made sense to have two different kinds since they do the same thing. The only difference is that 'icon' is implemented using an input type = image and image is implemented with a simple img tag.

2) Individual Kudo creation/configuration located under Content Mgmt

    * Here you would create the kudo. The name MUST not contain only contain alphanumeric characters and underscores. I need to throw in better validation for this.
    * Here you also select which content types that this individual kudo will show up for. This way you can have a special kudos for certain content types.
    * Under icons, you give the path to the images that you want to use. The three icons are used in the following manner:
      'give' = this is the icon you will see if you have never clicked on the kudo. It is always needed if icon or image type kudos were selected under site config
      'rescind' = this is the icon you will see if you have already given the kudo to someone AND the rescind option is enabled under site config
      'disabled' = this is the icon you will see if you have already given the kudo to someone AND the rescind option is disabled. This icon will also show if a user does not have permissions to give a kudo, either because they do not have permissions to 'give' or because it is a piece of content that they have posted, and in effect, can't pat themselves on the back.

* The kudos form may get bound to the node on view if the option is enabled under Site Config > Kudos.  The same thing applies to Comments.   The function can also be called directly. for example:


$kudos = kudos_widget_form('node', $node->nid)

or

$kudos = kudos_widget_form('comment', $a1->cid)

In addition to this, there are blocks that can be targeted to any node page which will cause the kudos to visible. 

----------------------------------
Kudos Blocks
----------------------------------

There are several blocks available out of the box, including:
* Kudos Node Form (this is for the granting of kudos)
* Kudos Node Stats (this is the kudos for a single piece of content)
* Kudos Node User Stats (this is for all content that the user has posted)


----------------------------------
Kudos Theme Functions 
----------------------------------

The way in which kudos appear on a site is highly customizable by overriding the theme functions which apply.  An effort has been made to give the utmost flexibility and as this module is still in active development, the effort continues.  That being said, the following are key theme functions which will probably have to be overriden for that special look your looking for:

theme_kudos_button_image
 - if the type of kudo selected was 'image', then this is the theme function to use to override each individual button.   It is important to keep some resemblance to the default handling (ie. IDs and the kudos specific classes that get assigned) as these are used by JQuery to capture the clicks, etc.

theme_kudos_button_icon
 - if the type of kudo selected was 'form icon' then this will be used.  (same rules apply as with theme_kudos_button_image)
 
 
theme_kudos_button_items
theme_kudos_button_items_block
 - this is the how the each button will be displayed relative to one another.   Note: 'theme_kudos_button_items_block' is called in cases where the Kudos Node Form block is included on a page
 
theme_kudos_account_stats_item
 - this handles how stats on a per user basis are displayed.  this function is called once for each individual kudo
theme_kudos_account_stats
 - this handles how all the account based elements are displayed relative to one another.
 
theme_kudos_content_stats_item
 - similar to the account_stats_item, this is called when looking at the stats of a particular piece of content
theme_kudos_content_stats
 - similar to the account_stats, this brings all the contents stats items together 

theme_kudos_content_summary_item
 - this is called when a summary (such as top weekly, etc) is tabulated and displayed.  similar to accounts_stats_item, this is called once for each kudo awarded

theme_kudos_content_summary
 - this is the function that is called to bring all the summary items together. 

----------------------------------
Kudos Hooks
----------------------------------
1) hook_kudosapi() 

This the core hook for the module which is invoked and can be invoked to give, rescind or get information about what kudos a piece of content has received.

2) hook_kudos_cache_periods()

3) hook_kudos_cache_period_info()

4) hook_kudos_cache_updated()

5) hook_kudos_summaryapi()


----------------------------------
Kudos Cron
----------------------------------

Cron will be run at a regular interval.   In admin/settings/kudos you can set how often stats for kudos should be updated via cron.  Each time this is to take place, the following procedure will take place:


1) Updates stats for each individual piece of content / user
	a) Retrieves the periods of time for which kudos stats should be determined per content / user
	b) Achieves this by invokeing the hook_kudos_cache_periods
	c) Gets specifics about each period by invoking hook_kudos_cache_period_info
2) After the initial run through of stats is completed, a summary report can be generated off of those stats.  Such a summary is for 'top 10 for the week' and so forth
	a) The hook_kudos_cache_updated is invoked
	b) The implementation of this hook should then call the function kudos_generate_summary_report for the period which it wants the report generated for. 
	c) This will in tern generate a report based on the findings of the original period stats from step one
3) After a summary report has been generated, hook_kudos_summaryapi will be invoked.
	a) This is where the awarding of points, notifications, and so forth should take place. 
	
	

Please note:

The module (and this readme doc) is in active development as everyday
