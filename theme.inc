<?php
//
//

/**
 * Implementation of theme_form_block_hidden()
 *
 * Default themeing for the kudos form which is hidden from users viewing their own content
 **/
function theme_kudos_form_block_hidden($vars) {
  $content = '';
  return $content;
}//end - function

/**
 * Implementation of theme_kudos_form()
 *
 * Default themeing for the kudos form
 */
function theme_kudos_form($form) {
  $content_type = $form['content_type']['#value'];
  $content_id = $form['content_id']['#value'];
  $widget_style = $form['#widget_style'];
  unset($form['#widget_style']);

  $content = '<div class="kudos-widget kudos-type-'. $content_type . ' kudos-id-'. $content_id .'">';
  //if we have kudos
  if (is_array($form['kudo'])) {
    $_themed_kudos = array();
    //foreach kudo
    foreach ($form['kudo'] as $key => $item) {
      //if this isn't a configuration parameter
      if (substr($key, 0,1) !== "#") {
        //switch on type of display
        switch (variable_get('kudos_format','kudos_submit')) {
          case 'kudos_submit' :
            //$form['kudo'][$key]['#type'] = "button";
            $_attributes = array();
            $_attributes['class'] = implode(' ', $item['#attributes']['class']);
            if ($item['#details']['enable'] !== "YES") {
              $_attributes['disabled'] = 'YES';
            }
            $_id = $item['#attributes']['id'];
            $_name = $form['kudo'][$key]['#name'];
            $_value = $form['kudo'][$key]['#value'];
            //$form['kudo'][$key]['#attributes'] = $_attributes;

            unset($form['kudo'][$key]);
            $form['kudo'][$key] = array(
              '#name' => $_name,
              '#id' => $_id,
              '#type' => 'submit',
              '#value' => $_value,
              '#attributes' => $_attributes,
            );

            if ($item['#details']['enable'] !== 'YES') {
              $form['kudo'][$key]['#disabled'] = TRUE;
            }

            $_themed_kudos[] = drupal_render($form['kudo'][$key]);
            break;
          case 'kudos_icons' :
            $vars = $item['#details'];
            $vars['#src'] = $item['#src'];
            $vars['#attributes'] = $item['#attributes'];
            if ($item['#disabled'] === "TRUE") {
              $vars['#attributes'] = $item['#attributes']['DISABLED'] = 'TRUE';
            }
            $_themed_kudos[] = _kudos_button_icon($vars);
            unset($form['kudo'][$key]);
            break;
          case 'kudos_image' :
            $vars = $item['#details'];
            $vars['#src'] = $item['#src'];
            $vars['#attributes'] = $item['#attributes'];
            if ($item['#disabled'] === "TRUE") {
              $vars['#attributes'] = $item['#attributes']['DISABLED'] = 'TRUE';
            }
            $_themed_kudos[] = _kudos_button_image($vars);
            unset($form['kudo'][$key]);
            break;
        }//end - switch on type of display
      }//end - if this isn't a configuration parameter
    }//end - foreach kudo

    //switch on the style of the widget
    switch ($widget_style) {
      case 'block' :
        $content .= theme('kudos_button_items_block', $_themed_kudos);
        break;
      default :
        $content .= theme('kudos_button_items', $_themed_kudos);
        break;
    }//end - switch on style for the widget
  }//end - if we have kudos

  $content .= drupal_render($form);
  $content .= '</div>';

  return $content;
}//end - function

/**
 * Implementation of theme_kudos_buttons_block()
 *
 * Default themeing for how to display the entire kudos form block... useful if there is a need to wrap the form block
 */
function theme_kudos_buttons_block($content) {
  return $content;
}//end - function

/**
 * Implementation of theme_kudos_buttons()
 *
 * Default themeing for how to display the entire kudos form... useful if there is a need to wrap the form.
 * This is the version that gets included automatically to nodes and comments
 */
function theme_kudos_buttons($content) {
  return $content;
}//end - function

/**
 * Implementation of theme_kudos_button_items_block()
 *
 * Default themeing for how to display all the elements together
 * This is the version that is included on a page using the block available in the admin
 */
function theme_kudos_button_items_block($items) {
  $content = implode('',$items);
  $content .= '
    <div class="kudos_response_message"></div>
  ';
  return $content;
}//end - function

/**
 * Implementation of theme_kudos_button_items()
 *
 * Default themeing for how to display all the elements together
 */
function theme_kudos_button_items($items) {
  $content = implode('',$items);
  $content .= '
    <div class="kudos_response_message"></div>
  ';
  return $content;
}//end - function

/**
 * Implementation of theme_kudos_button_image()
 *
 * Default themeing for an individual button image
 **/
function theme_kudos_button_image($vars) {
  $_attrs = "";
  //format the attributes of the button
  foreach ($vars['#attributes'] as $name => $values) {
    $_attrs .= ' '. $name .'="'. (is_array($values) ? implode(' ', $values) : $values) .'"';
  }

  $output = '<img src="'. $vars['#src'] .'" alt="'. $vars['label'] .'" name="'. $vars['name'] .'" value="'. $vars['label'] .'" '. $_attrs . ' />';
  return $output;
}//end - function

/**
 * Implementation of theme_kudos_button_icon()
 *
 * Default themeing for an individual button icon
 **/
function theme_kudos_button_icon($vars) {
  $_attrs = "";
  //format the attributes of the button
  foreach ($vars['#attributes'] as $name => $values) {
    $_attrs .= ' '. $name .'="'. (is_array($values) ? implode(' ', $values) : $values) .'"';
  }

  $output = '<input type="image" src="'. $vars['#src'] .'" alt="'. $vars['label'] .'" name="'. $vars['name'] .'" value="'. $vars['label'] .'" '. $_attrs . ' />';
  return $output;
}//end - function

/**
 * Implementation of theme_kudos_response_message()
 *
 * Default themeing for how the message will display after a kudo is given/rescinded
 */
function theme_kudos_response_message($vars) {
  $content = '';

  if ($vars['success'] === "YES") {
    $content = t('The kudo was %s successfully.',array('%s' => $vars['op']));
  } else {
    $content = t('The kudo was %s successfully.',array('%s' => $vars['op']));
  }

  return $content;
}//end - function

/**
 * Default theming for each account stats item
 **/
function theme_kudos_account_stats_item($vars) {
  $content = $vars['details']['label'] . '(' . $vars['details']['num_given'] . ')';
  return $content;
}//end - function

/**
 * Default theming for putting all the account stats items together
 **/
function theme_kudos_account_stats($vars) {
  $content = '<div class="kudos_account_stats">
    <span class="heading">Users Kudos</span>';
  $content .= theme('item_list', $vars['items']);
  $content .= '</div>';
  return $content;
}//end - function

/**
 * Default theming for each content summary item
 **/
function theme_kudos_account_summary_item($vars) {
  $content = $vars['details']['content_type'] . "/" . $vars['details']['content_id'] . '(' . $vars['details']['num_given'] . ')';
  return $content;
}//end - function

/**
 * Default theming for putting all the content summary items together
 **/
function theme_kudos_account_summary($vars) {
  $content = '<div class="kudos_account_summary">
    <span class="heading">Top Users awarded Kudos</span>';
  $content .= theme('item_list', $vars['items']);
  $content .= '</div>';
  return $content;
}//end - function

/**
 * Default theming for each content stats item
 **/
function theme_kudos_content_stats_item($vars) {
  $content = $vars['details']['label'] . '(' . $vars['details']['num_given'] . ')';
  return $content;
}//end - function

/**
 * Default theming for putting all the content stats items together
 **/
function theme_kudos_content_stats($vars) {
  $content = '<div class="kudos_content_stats">
    <span class="heading">Kudos Awarded</span>';
  $content .= theme('item_list', $vars['items']);
  $content .= '</div>';
  return $content;
}//end - function

/**
 * Default theming for each content summary item
 **/
function theme_kudos_content_summary_item($vars) {
  $content = $vars['details']['label'] . " [". $vars['details']['id'] . "] " . $vars['details']['content_type'] . "/" . $vars['details']['content_id'] . '(' . $vars['details']['num_given'] . ')';
  return $content;
}//end - function

/**
 * Default theming for putting all the content summary items together
 **/
function theme_kudos_content_summary($vars) {
  $content = '<div class="kudos_content_summary">
    <span class="heading">Top Kudos Awarded to Content</span>';
  $content .= theme('item_list', $vars['items']);
  $content .= '</div>';
  return $content;
}//end - function

/**
 * Default handling for the jquery interface
 **/
function theme_kudos_submit($vars) {
  $output = "";
  foreach ($vars as $name => $value) {
    switch ($name) {
      case 'success_message' :
        $output .= sprintf('<%s><![CDATA[%s]]></%s>', $name, $value, $name);
        break;
      default :
        $output .= sprintf('<%s>%s</%s>', $name, $value, $name);
        break;
    }//end - switch
  }//end - foreach
  $output = '<response>
' . $output ."</response>";

  return $output;
}//end - function

/**
 * Default themeing for the admin configuration listing
 **/
function theme_kudos_admin_config_listing($vars) {
  $items = array();
  $content = '';

  if (is_array($vars)) {
    //foreach kudo
    foreach ($vars as $key => $info) {
      $item = array();
      $item['data'] = l('Edit', 'admin/content/kudos/' . $info['id']) . " " . $info['label'];
      $items[] = $item;
    }//end - foreach
  }

  if (count($items) > 0) {
    $content = '<fieldset><legend>Existing Kudos</legend>';
    $content .= theme('item_list', $items);
    $content .= '</fieldset>';
  }

  return $content;
}//end - function
