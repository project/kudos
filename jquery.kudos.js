//
//
/**
 * When the page is ready, bind click event to all the appropriate kudo buttons/icons so they will be ready to process
 **/
$(document).ready(function() {
  $('form.kudos-widget .kudos_clicker')
    .not('.kudos_none')
      .bind("click", function(){
        //determine what form this is based on the id of the button.
        //note: because themers can change the structure of buttons, its not possible to simply iterate based on parent/child relationships
        _tmp = $(this).attr('id').split('-');
        _ct = _tmp[2];
        _cid = _tmp[3];
        v_form_id = "kudos-form-" + _ct + "-" + _cid;

        //get the additional data for the operation of this kudo
        sub_action = $('#' + v_form_id).attr('action');
        v_button_format = $('#' + v_form_id).find('input[@name=button_format]').val();
        v_content_type = $('#' + v_form_id).find('input[@name=content_type]').val();
        v_content_id = $('#' + v_form_id).find('input[@name=content_id]').val();
        v_form_token = $('#' + v_form_id).parent().find('input[@name=form_token]').val();
        v_form_id = $('#' + v_form_id).find('input[@name=form_id]').val();

        v_button_id = $(this).attr('id');
        v_kudo_name = $(this).attr('name');
        v_kudo_type = (v_button_format == "kudos_image" ? $(this).attr('alt') : $(this).val());

        //check what operation is being performed
        v_operation = 'none';
        if ($(this).is(".kudos_give")) { v_operation = 'give'; }
        else if ($(this).is(".kudos_rescind")) { v_operation = 'rescind'; }

        //handle the kudo process
        kudos_post(v_operation, v_content_type, v_content_id, v_kudo_name, v_kudo_type, v_form_token, v_form_id, v_button_id);
        return false;
      })//end - click event function
      .css('cursor', 'pointer');//if we make it add a click event, make sure there is a pointer style attached
});//end - on ready

/**
 * Handles the actual posting of data and parsing response
 **/
function kudos_post(v_operation, v_content_type, v_content_id, v_kudo_name, v_kudo_type, v_form_token, v_form_id, v_button_id) {
  //alert('OP:' + v_operation + '\nCT:' + v_content_type + "\nCI: " + v_content_id+ "\nNM: " +v_kudo_name + "\nTY: " + v_kudo_type+ "\nTO: " + v_form_token+ "\nFI:" + v_form_id+ "\nBT: " + v_button_id + "");
  v_kudo_type = (v_kudo_type == "undefined" ? '' : v_kudo_type);
  $.post( sub_action,
          { operation: v_operation,
            content_type: v_content_type,
            content_id: v_content_id,
            kudo_name: v_kudo_name,
            kudo_type: v_kudo_type,
            form_token: v_form_token,
            form_id: v_form_id,
            button_id: v_button_id,
            button_format: v_button_format
          },
          function(data){
            //alert(data);
            v_operation = $("operation",data).text();
            v_success = $("success",data).text();
            v_success_message = $("success_message",data).text();
            v_success_message_display_time = $("success_message_display_time",data).text();
            v_enable =  $("enable",data).text();
            v_format = $("format",data).text();
            v_kudo_name = $("kudo_name",data).text();

            v_next_status = $("next_status",data).text();
            v_button_id = $("button_id", data).text();
            v_button_format = $("button_format", data).text();
            v_button_image = $("button_image", data).text();
            v_allow_rescind = $("allow_rescind",data).text();

            //switch on interface format
            switch (v_button_format) {
              case 'kudos_submit' : //for standard buttons
                break;
              case 'kudos_icon' : //for the use of icons
              case 'kudos_image' : //for the use of image
                //if a changed image is returned
                if (v_button_image != '') {
                  $('#' + v_button_id).attr('src',v_button_image);
                }
                break;
            }//end - switch on interface format

            //switch depending on success of operation... this is independent of format
            switch (v_success) {
              case 'TRUE' :
                //if rescind is allowed
                if (v_allow_rescind == "TRUE") {
                  //switch on operation
                  switch (v_operation) {
                    case 'give' :
                      $('#' + v_button_id).removeClass('kudos_give');
                      $('#' + v_button_id).addClass('kudos_rescind');
                      break;
                    case 'rescind' :
                      $('#' + v_button_id).removeClass('kudos_rescind');
                      $('#' + v_button_id).addClass('kudos_give');
                      break;
                  }//end - switch on operation
                } else { //else - just disable the button/image
                  //switch on operation
                  switch (v_operation) {
                    case 'give' :
                      $('#' + v_button_id).removeClass('kudos_give');
                      $('#' + v_button_id).addClass('kudos_none');
                      $('#' + v_button_id)
                        .unbind('click') //remove the click event since this kudo is now disabled
                        .css('cursor', 'default');//since this is no longer clickable, remove the pointer
                      break;
                    case 'rescind' :
                      $('#' + v_button_id).removeClass('kudos_rescind');
                      $('#' + v_button_id).addClass('kudos_none');
                      $('#' + v_button_id)
                        .unbind('click') //remove the click event since this kudo is now disabled
                        .css('cursor', 'default');//since this is no longer clickable, remove the pointer
                      break;
                  }//end - switch on operation
                }//end - if rescind is allowed

                //switch on button enable
                switch (v_enable) {
                  case 'NO' :
                    $('#' + v_button_id).attr('disabled','true');
                    break;
                  case 'YES' :
                    $('#' + v_button_id).removeAttr('disabled');
                    break;
                }//end - switch on enable
                if (v_success_message != "") {
                  if (v_success_message_display_time != 0) {
                    $('#' + v_button_id).parents('form').find('.kudos_response_message').show().html(v_success_message);
                    //note: change fadeOut to hide because fadeOut does not appear to be compatible with fadeOut
                    if (v_success_message_display_time > 0) { //a value of -1 means indefinitely
                      setTimeout("$('#' + v_button_id).parents('form').find('.kudos_response_message').empty().hide();", v_success_message_display_time);
                    }
                  }
                } else {
                  alert(" Kudo saved successfully. ");
                }
                break;
              case 'FALSE' :
                if (v_success_message != "") {
                  if (v_success_message_display_time != 0) {
                    $('#' + v_button_id).parents('form').find('.kudos_response_message').show().html(v_success_message);
                    //note: change fadeOut to hide because fadeOut does not appear to be compatible with fadeOut
                    if (v_success_message_display_time > 0) { //a value of -1 means indefinitely
                      setTimeout("$('#' + v_button_id).parents('form').find('.kudos_response_message').empty().hide();", v_success_message_display_time);
                    }
                  }
                } else {
                  alert(" Kudo not saved successfully. ");
                }
                break;
              default:
                alert('Unknown Error');
                break;
            }//end - switch depending on success...
          }//end - post return function
        );//end - post function
}//end - function
